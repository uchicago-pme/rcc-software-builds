# Scientific Software Build Scripts for RCC Resources

A collection of build scripts for scientific software commonly used by the researach 
groups at the University of Chicago's Institute for Molecular Engineering. Each 
build is tailored for use on the University of Chicago's Research Computing Center
resources. 

## Organization of this Repository

Each software build is contained within a folder of the same name as the software.
In addition to the build script for the software, each software build folder 
contains a README file with details for building the software and an `examples`
subfolder, which contains an example input file(s) along with the corresponding
SLURM sbatch job submission script `job.sbatch`.

The list of software builds is as follows: 
* **gromacs** -- Build for [Gromacs](http://www.gromacs.org/). See the [Gromacs build README file](gromacs/README.md)
          for details on building the software.
* **lammps** -- Build for [LAMMPS](https://lammps.sandia.gov/). See the [LAMMPS build README file](lammps/README.md)
          for details on building the software.
* **qbox** -- Build for [Qbox](http://qboxcode.org). See the [QBox build README file](qbox/README.md)
          for details on building the software.
* **qe-west** -- Build for [Quantum Espresso](https://www.quantum-espresso.org/) 
                 and [WEST](http://www.west-code.org/). One can choose to build 
                 Quantum Espresso or Quantum Espresso coupled with WEST. See the
                 [QE WEST build README file](qe-west/README.md) for more details 
                 about the build.
* **tf-cpu** -- Build for [Tensorflow CPU](https://www.tensorflow.org/). Build enables highest avdvanced vector extensions available for the target platform. See the [TF-CPU README file](tf-cpu/README.md) for details of the build.  


Questions, comments or issues should be sent to jhskoneATuchicago.edu

## Other Considerations

### Advanced vector extensions and the build microarchitecture

There are several generations of hardware of various microarchitectures (i.e. ivybridge, broadwell, skylake, cascade lake) in different partitions on midway. If you build 
an application with advanced vector extensions, you can target the highest available for the microarchitecture with `-xHost`, but you should be aware that this may result 
in a binary that is not usable on older microarchitectures. For example, if you build on nodes with Intel Cascade Lake CPUs, you can use AVX512 extensions, but since this 
vector extension is not available on older microarchitectures like Broadwell and Sandy Bridge, your binary can only work on Cascade Lake and Skylake nodes. If you want to
have a binary that is compatible on multiple microarchitectures, do not use `-xHost` and instead use the explicit extension set for the oldest microarchitecture that the
binary should be compatible with.
