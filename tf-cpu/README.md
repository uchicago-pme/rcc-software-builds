## Instructions on Building Tensorflow from Source 

### CPU Version Instructions

* Load the Anaconda module you intend to use

  `module load Anaconda3/2018.12`
  
* Create the conda environment or use existing envrionment. The python install of
  Tensorflow requires the conda environment you create has the following packages: 

    * numpy
    * scipy
    * keras-applications
    * keras-base
    * cython (maybe you do not need this one. I am not sure. I throw it in for sake of time). 

   `conda create --name <name-of-env> python=<python version> numpy scipy keras-applications keras-base cython`

**Note:** The scipy and numpy need to be the most recent versions. If you can not 
get the most recent from conda pkg manager then you will have to install from pip 
w/ `pip install --upgrade scipy  `

Its a real pain in the rear to set things up, 
begin the tensorflow build and find it bomb half way through because some 
dependency is not the right version. If this happens clear your bazel cache. See
note below about this in the Considerations section. 

* Build the bazel installer  (probably this could be put in module system)
  * Modify the bazel-build.sh build script 
  * Run the build script (preferablly from the build partition)

* Build TF 
  * modify the tf-keras-build.sh script
  * Run the script (or do it in sections -- this needs to be built into make 
    script so can make download, make config, make install, make test)  

## Considerations
* Build fails: 
If your tf build fails and you need to start over, its a good idea to clear the
bazel cache  in ~/.cache/bazel 
* First time building: 
If this is your first time building from source then probably you can expect to 
lose a full day of time to this. :( 

