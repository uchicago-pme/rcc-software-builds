#!/bin/bash
#
# TENSORFLOW CPU BUILD FROM SOURCE SCRIPT
# REQUIRES BAZEL IS BUILT AND YOU SET ITS LOCATION

# SET CURRENT DIR
 BASE_DIR=$(pwd)
#
#  ANACONDA MODULE TO USE
  py_module="Anaconda3/2018.12"
# SET CONDA ENV TO USE
  conda_env="tf-cpu-1.13.1"
#
# SET TENSORFLOW VERSION
 export tensorflow_version="1.13.1"

# LOAD BUILD DEPENDENT MODULES
# module load gcc/6.2   # Necessary for C++ bindings NOT python tf build
 module load $py_module
 source activate $conda_env
 module load cmake/3.11.0 
 module load java  # this is required for bazel 

#
# SET TF INSTALL DIR 
# only needed for cloning and building. We will
# install tf as a whell to the lib of the environment
# set above.   
 export tensorflow_root=$BASE_DIR/tensorflow
#
# SET THE BAZEL BUILD DIR LOCATION 
 export bazel_ver=0.19.0
 export bazel_dir=bazel-$bazel_ver
 export PATH=$bazel_dir/output:$PATH
# echo "THE BAZEL BINARY IS LOCATED AT $bazel_dir/output" 
#
# CLONE TF GIT REPO AND CHECKOUT THE DESIRED VERSION
  git clone https://github.com/tensorflow/tensorflow tensorflow  
  cd tensorflow
  git checkout tags/v$tensorflow_version 
#
# THE FOLLOWING IS IF THIS BUILD IS MEANT TO BE SCRIPTED
# I FEEL ITS BETTER TO MANUALY RUN THE CONFIGURE SCRIPT 
# TO ENSURE THINGS ARE SET AND CORRECT THEM IF THEY ARE NOT

# cp $BASE_DIR/input $tensorflow_root
# note configure will set by default -march=native if you specify no optimization options
# we can modify/augment this later when running the bazel build of tensorflow
# ./configure < input 

#
# BUILD OF TF 
# MAKE SURE YOU RUN THIS FROM A COMPUTE NODE BC BAZEL IS RESOURCE GREEDY
# the --local_resources is meant to constrain bazel in some aspecst of build
# but I am not able to fully control it. 40480 is 40 GB of ram and 20. is meant to be 20 cores and 2 is related to network use. 
#
# SET BUILD OPTIMIZATION OPTIONS
  BAZEL_OPTS="--config=opt --verbose_failures --copt=-mfpmath=both --copt=-mavx --copt=-mavx2 --local_resources 40480,20.0,2.0"
#
  echo "BUILDING TENSORFLOW PYTHON LIB WITH OPTIONS:"
  echo "$BAZEL_OPTS"
#
# BEGIN THE LENGTHY BUILD
  bazel build $BAZEL_OPTS  //tensorflow/tools/pip_package:build_pip_package
# 
# CREATE PACKAGE TO THEN INSTALL AS WHEEL 
  ./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
# 
# INSTALL WHEEL TO CURRENT PYTHON LIB
  pip install /tmp/tensorflow_pkg/tensorflow-$tensorflow_version-cp36-cp36m-linux_x86_64.whl 
#
# EOF
