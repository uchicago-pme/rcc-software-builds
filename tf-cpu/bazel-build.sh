#!/bin/bash
#
# BAZEL BUILD SCRIPT
#
# SET CURRENT DIR
  BASE_DIR=$(pwd)
# ANACONDA MODULE TO USE
  py_module="Anaconda3/2018.12"
# SET CONDA ENV TO USE
  conda_env="tf-cpu-1.13.1"

# LOAD BUILD DEPENDENT MODULES
 module load $py_module
 source activate $conda_env
 module load java  # this is for bazel 

# SET BAZEL VERSION 
# FOR TF >= 1.13.1 must use baze ver >= 0.19.0
 export bazel_ver=0.19.0
 export bazel_dir=$tensorflow_root/bazel-$bazel_ver
#
# GET BAZEL SOURCE AND UNPACK
# INSTALL bazel version 
  wget https://github.com/bazelbuild/bazel/releases/download/$bazel_ver/bazel-$bazel_ver-dist.zip
  mkdir bazel-$bazel_ver
#
# INSTALL BAZEL
  cd bazel-$bazel_ver
  unzip ../bazel-$bazel_ver-dist.zip
  ./compile.sh
#
# LOCATION OF BAZEL
  echo "BAZEL BINARY IS LOCATED: $BASE_DIR/bazel-$bazel_ver/output"
# EOF
