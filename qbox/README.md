 ![ |small ](imgs/qb.png)

# QBox build script 

This build script downloads, unpacks and builds the Qbox code 
http://qboxcode.org/download/

## Default Build

To default build currently version 1.66.3

To execute the default build: 
```bash
./qbox-build.sh 
```

If the install is successful this will be indicated along with the 
full path to the binary and the list of libraries linked to the binary.

## Adjusting the Default Build

The build script itself should be well commented that it should be 
easy to understand where to make changes to adjust the build script
to meet your particular installation requirements on RCC resources. 
A list is provided below that details the more common variables
that one may wish to adjust in the build script. 

* SOFTVERS   -- This is the version of QBox to install. The default is 1.66.2

* TARGET     -- The target make file to use in building qbox. There are two
                included with this repository. TARGET can either be: 
                midway-fftw3
                midway-mklfftw3
                The difference between the two is where the FFTW3 library is 
                linked from. midway-mklfftw3 uses the mkl fftw3 lib whereas
                midway-fftw3 uses the source built fftw3 (on midway from a fftw3 module).

* PREFIX     -- The target installation path. The default will install the QBox
                binary in the /bin folder of the current working directory. If you 
                wish to change this be sure to also update the path to the QBox
                executable in your sbatch submission script. 
* COMP_MOD   -- The compiler module to use. The default is intelmpi/2018.2.199+intel-18.0

* MATH_MOD   -- The math library module to use. The default is to use mkl/2018.up2

* FFTW3_MOD  -- The FFTW3 module to use if not linking against to MKL for fftw3 libs.
                The default is fftw3/3.3.5+intelmpi-5.1+intel-16.0 
                If you use a FFTW library be sure to also use the same compiler version
                that was used to build the FFTW3 lib when setting COMP_MOD for the 
                qbox build.

* XERCES_MOD -- The xerces module to use. The default is xerces/3.1.4         

## Example Job Submission Scripts and Input files

The `examples` folder contains a representative qbox calculation. 

In the example folder is an input file for the qbox job and the corresponding
SLURM sbatch jobs submission script `job.sbatch`. 
You should only need to change the `BIN_DIR` env variable in the job.sbatch
script to match the location chosen for the installation of the binaries. 

## Other Considerations

You should be aware that nodes part of midway2 use the midway2 module system while
nodes that are part of midway1 use the midway1 module system. 
The default set of compilers and libraries used for this build script can be found 
in both the midway1 and midway2 module system. You should run your qbox executable
on the same hardware that you used to build it. If you build the executable on an
ivybridge node then you should use it on ivybridge nodes.  Should you change the modules used for 
the build make sure they exist on the target system.          

It's not necessary to add the -liomp5 to the `LIBS` of the make file as this is
picked up and linked regardless. 
