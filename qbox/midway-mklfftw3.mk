#############################
# QBOX MIDWAY MAKE FILE     # 
# FFTW3 LIB LINKED FROM MKL #
#############################
PLTOBJECTS = readTSC.o

XERCESCDIR = $(XERCESCROOT)
FFTWDIR = $(MKLROOT)

CXX=mpicxx
LD=$(CXX)

OPT = -O3  -ipo
OMP = -qopenmp

PLTFLAGS += $(OPT) $(OMP) -DIA32 -DUSE_FFTW3 -D_LARGEFILE_SOURCE \
           -D_FILE_OFFSET_BITS=64 -DUSE_MPI -DSCALAPACK -DADD_ \
           -DAPP_NO_THREADS -DXML_USE_NO_THREADS -DUSE_XERCES \
           -DMPICH_IGNORE_CXX_SEEK

INCLUDE = -I$(FFTWDIR)/include -I$(XERCESCDIR)/include

CXXFLAGS= -D$(PLT) $(INCLUDE) $(PLTFLAGS) $(DFLAGS)

LIBPATH = -L$(XERCESCDIR)/lib -L$(FFTWDIR)/lib  -L$(MKLROOT)/lib/intel64

LIBS =  $(OMP) -lxerces-c  -mkl \
         -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core \
         -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm

LDFLAGS = $(LIBPATH) $(LIBS)
