#!/bin/bash

#####################
# QBOX BUILD SCRIPT #
#####################
#
# 
# SET QBOX SOFTWARE VERSION:
SOFTVERS=1.66.3
#
# SET THE MIDWAY MAKE FILE TARGET
# There are two midway make files. 
# midway-fftw3    -- uses compiled fftw3 lib
#                       you must set FFTW3_MOD below
# midway-mklfftw3 -- uses mkl for fftw3 lib
export TARGET="midway-mklfftw3"
#
# SET WHERE TO INSTALL THE QBOX BINARY
# DEFAULT IS TO BUILD IN THE CURRENT DIRECTORY
CURR_DIR=`pwd`
PREFIX=$CURR_DIR/bin
mkdir -p  $PREFIX
#
# SET NUMBER OF PARALLEL MAKE TASKS FOR BUILD
CORES_TOT=`cat /proc/cpuinfo | grep -c processor`
CORES_DIV=4
NCORES=$(( CORES_TOT / CORES_DIV))
#
# SET THE MODULES NEEDED TO BUILD: 
# 
# COMPILER AND MPI LIB MODULES
# 
# COMP_MOD="intelmpi/5.1+intel-16.0" # USED FOR THE FFTW3 BUILD
COMP_MOD="intelmpi/2018.2.199+intel-18.0"
#
# MATH LIBRARY AND XERCES LIB
MATH_MOD="mkl/2018.up2"
XERCES_MOD="xerces/3.1.4"
#
# FFTW3 LIBRARY
# IF LINKING MKL FOR FFTW3 LIBS
# THEN YOU DO NOT NEED TO SET FFTW3_MOD
FFTW3_MOD="fftw3/3.3.5+intelmpi-5.1+intel-16.0"
#
# LOAD MODULES
if [ "$TARGET" = "midway-fftw3" ] ; then
   module load $COMP_MOD $MATH_MOD $XERCES_MOD $FFTW3_MOD
elif [ "$TARGET" = "midway-mklfftw3" ] ; then
   module load $COMP_MOD $MATH_MOD $XERCES_MOD
else 
   echo "ERROR WITH THE MAKE TARGET "
   echo "OPTIONS FOR MIDWAY MAKE TARGET ARE: "
   echo "midway-fftw3  or  midway-mklfftw3"
fi
#
# GET THE QBOX SOURCE AND UNPACK IT:
IFS='.' read -r -a array <<< "$SOFTVERS"
MAJOR_SUB1="${array[0]}"
MAJOR_SUB2="${array[1]}"
MINOR="${array[2]}"
RELEASE=rel${MAJOR_SUB1}_${MAJOR_SUB2}_${MINOR}

wget https://github.com/qboxcode/qbox-public/archive/$RELEASE.tar.gz
tar xzvf $RELEASE.tar.gz
mv qbox-public-$RELEASE qbox-$SOFTVERS
#
# BEGIN BUILD
#
MIDWAY_MAKE="$TARGET.mk"
cp $MIDWAY_MAKE qbox-$SOFTVERS/src
#
# NOW MAKE BUILD QBOX:
cd qbox-$SOFTVERS/src
make clean
make -j $NCORES
install -d $PREFIX -m 755
install -m 755 qb $PREFIX
#
# CHECK THAT THE qb EXECUTABLE EXISTS
if [ -f $PREFIX/qb ]; then
    
    echo "**********************************"
    echo "**  COMPILATION WAS SUCCESSFUL  **"
    echo "**********************************"
    echo " "
    echo "THE QBOX BINARY INSTALL LOCATION: "
    echo " $PREFIX                          "
    echo ""
    echo "REQUIRED MODULES:"
    if [ "$TARGET" = "midway-fftw3" ] ; then
       echo " $COMP_MOD $MATH_MOD $FFTW3_MOD "
    else 
       echo " $COMP_MOD $MATH_MOD "
    fi 
    echo ""
else 
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    echo "!! COMPILATION WAS UNSUCCESSFUL !!"
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
fi
#
# EOF
