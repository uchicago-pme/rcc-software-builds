#!/bin/bash
#
# SET GROMACS VERSION TO BUILD
GROMACS_VERSION=2018.6
#
# TURN ON/OFF PLUMED BUILD WITH GROMACS -- THIS CURRENTLY IS NOT USED
PLUMED=true
#
# SET THE INSTALL DIRECTORY
CURR_DIR=`pwd`
INSTALL_DIR=$CURR_DIR/gromacs-$GROMACS_VERSION
#
# GET GROMACS VERSION SOURCE
mkdir build ; cd build
wget http://ftp.gromacs.org/pub/gromacs/gromacs-$GROMACS_VERSION.tar.gz
tar xzvf gromacs-$GROMACS_VERSION.tar.gz
cd gromacs-$GROMACS_VERSION
#
# SET THE MODULES NEEDED TO BUILD: 
#
# CUDA LIBRARY -- FOR MIDWAY1 THE SL6 ONLY SUPPORTS UP TO cuda v 8.0
#                 CONSEQUENTLY cuda 8.0 ONLY SUPPORTS INTEL 15 AND 16 COMPILERS
CUDA_MOD="cuda/8.0"
# 
# COMPILER AND MPI LIB MODULES
COMP_MOD="intelmpi/5.1+intel-16.0"
#
# MATH LIBRARY
MATH_MOD="mkl/11.3"
#
# CMAKE MODULE FOR BUILD
CMAKE="cmake"
#
# LOAD MODULES
module load $COMP_MOD $MATH_MOD $CUDA_MOD $CMAKE
#
# SET NUMBER OF CORES TO USE IN PARALLEL MAKE
#
CORES_TOT=`cat /proc/cpuinfo | grep -c processor`
CORES_DIV=4
NCORES=$(( CORES_TOT / CORES_DIV))
#
# SET COMPILERS -- USING INTEL HERE
export CC=icc
export CXX=icpc
#
# SET AVX VERSION
 echo "BUILDING WIHT AVX2"
 AVX=AVX2_256

#        [ -d build ] && rm -rf build
   mkdir build-$SOFTVERS
   cd build-$SOFTVERS
   cmake ../ -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR -DGMX_GPU=ON -DGMX_MPI=ON \
         -DCUDA_TOOLKIT_ROOT_DIR=$CUDA_HOME -DGMX_BUILD_OWN_FFTW=ON -DGMX_SIMD=$AVX
   make -j $NCORES
   make install

#
# CHECK THAT THE gmx_mpi  EXECUTABLE EXISTS
if [ -f $INSTALL_DIR/bin/gmx_mpi ] ; then
    echo "**********************************"
    echo "**  COMPILATION WAS SUCCESSFUL  **"
    echo "**********************************"
    echo " "
    echo "THE GROMACS BINARIES INSTALL LOCATION: "
    echo " $INSTALL_DIR/bin "
    echo ""
    echo "REQUIRED MODULES:"
    echo " $COMP_MOD $MATH_MOD $CUDA_MOD "
    echo ""
      else
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    echo "!! COMPILATION WAS UNSUCCESSFUL !!"
    echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
fi
#
# EOF
