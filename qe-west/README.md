 ![ |small ](imgs/qe-west.png)

# Quantum Espresso build with support for WEST and ENVIRON packages

This build script downloads, unpacks and builds [Quantum Espresso](https://www.quantum-espresso.org/) (QE) and optionally [WEST](http://www.west-code.org/) and or [ENVIRON](http://www.quantum-environment.org/home.html). 

## Default Build

Without any changes, the script will build Quantum Espresso version 6.1.0 and WEST version 3.1.1  
The Quantum Espresso build is for the pw.x, ph.x and pp.x executables. The WEST build is for the wstat.x and wfreq.x binaries. 

To execute the default build: 
```bash
./qe-pkg-build.sh
```

If the install is successful this will be indicated along with the 
full path to the binary and the list of libraries linked to the binary.

## Adjusting the Default Build

The build script is commented so that it should be easy to understand
where to make changes to adjust the build script to meet your 
particular installation requirements on RCC resources. A list 
is provided below that details the more common variables that
one may wish to adjust in the build script. 

* QE_VERSION    -- This is the version of Quantum Espresso to install. The default is 6.1.0

* WEST_VERSION  -- This is the version of WEST to install. The default is 3.1.1

* BUILD_WEST    -- A logical to turn on/off the build for WEST. The default is true (builds WEST)

* ENVIRON_VERS  -- This is the version of ENVIRON to install. The default is 1.1

* BUILD_ENVIRON -- A logical to turn on/off the build for ENVIRON. The default is false (does not build ENVIRON)

* INSTALL_DIR   -- The target installation path. The default will install the QE
                binary in the /bin folder of the current working directory. If you 
                wish to change this be sure to also update the path to the QE
                executable in your sbatch submission script. 

* COMP_MOD      -- The compiler module to use. The default is intelmpi/5.1+intel-16.0

* MATH_MOD      -- The math library module to use. The default is to use mkl/2017.up4

If you want to install ALL the binaries for espresso instead of just pw.x, pp.x
and ph.x, you need to uncomment the line after the configure script for `make all`. 

## Example Job Submission Scripts and Input files

The `examples` folder contains three representative types of calculations: 
* pwscf
* phonon
* west 

In each representative example folder are the corresponding input files for the 
representative job and the corresponding SLURM sbatch jobs submission script 
`job.sbatch`. You should only need to change the `BIN_DIR` env variable in the 
job.sbatch script to match the location chosen for the installation of the binaries. 

## Other Considerations

You should be aware that nodes part of midway2 use the midway2 module system while
nodes that are part of midway1 use the midway1 module system. 
The default set of compilers and libraries used for this build script can be found 
in both the midway1 and midway2 module system. You should run your espresso executable
on the same hardware that you used to build it. If you build the executable on an
ivybridge node then you should use it on ivybridge nodes.  Should you change the
modules used for the build make sure they exist on the target system.  

