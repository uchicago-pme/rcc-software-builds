# LAMMPS build script 
![ |small ](imgs/lammps.gif)

This build script downloads, unpacks and builds [LAMMPS](https://lammps.sandia.gov/). 

## Default Build

Without any changes, the script will build LAMMPS version lammps-17Nov16


To execute the default build: 
```bash
./lammps-build.sh 
```

If the install is successful this will be indicated along with the 
full path to the binary and the list of libraries linked to the binary.

## Adjusting the Default Build

The build script is commented so that it should be easy to understand
where to make changes to adjust the build script to meet your 
particular installation requirements on RCC resources. A list 
is provided below that details the more common variables that
one may wish to adjust in the build script. 

* LMP_VERSION  -- This is the version of LAMMPS to install. The default is lammps-17Nov16

* LIB_BUILD    -- A logical string to turn on/off the build of supporting libraries (e.g. atc awpmd colvars, etc.) 

* PREFIX       -- The target installation path. The default will install lammps
                binaries in the /bin folder of the current working directory. If you 
                wish to change this be sure to also update the path to the lammps
                executable in your sbatch submission script. 
* COMP_MOD      -- The compiler module to use. The default is intelmpi/5.1+intel-16.0

* MATH_MOD      -- The math library module to use. The default is to use mkl/2017.up4

## Example Job Submission Scripts and Input files

In each representative example folder are the corresponding input files for the 
representative job and the corresponding SLURM sbatch jobs submission script 
`job.sbatch`. You should only need to change the `BIN_DIR` env variable in the 
job.sbatch script to match the location chosen for the installation of the binaries. 

## Other Considerations

You should be aware that nodes part of midway2 use the midway2 module system while
nodes that are part of midway1 use the midway1 module system. 
The default set of compilers and libraries used for this build script can be found 
in both the midway1 and midway2 module system. You should run your lammps executable
on the same hardware that you used to build it. If you build the executable on an
ivybridge node then you should use it on ivybridge nodes. Should you change the
modules used for the build make sure they exist on the target system.  

