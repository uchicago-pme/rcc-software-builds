#!/bin/bash
# 
# SET LAMMPS VERSION  AND WHETHER TO BUILD PACKAGE LIBRARIES
# No plumed patch with this version -- if you want it let me know
#
 LMP_VER="lammps-17Nov16"
 LIB_BUILD="no"
# LIB_BUILD="yes"
 CURR_DIR=`pwd`
# 
# SET LAMMPS INSTALL DIRECTORY (PREFIX) 
#
 PREFIX=$CURR_DIR
#
# SET COMPILER AND MATH LIB MODULES
#
COMP_MOD="intelmpi/5.1+intel-16.0"
MATH_MOD="mkl/2017.up4"
#
# LOAD MODULES
#
module load $COMP_MOD $MATH_MOD 
module li
#
echo "*****************************************"
echo " LAMMPS BUILD `date` " 
echo "INSTALL DIR:" $PREFIX
echo "*****************************************"
# 
# DOWNLOAD LAMPPS and UNTAR
#
if [ ! -e $LMP_VER ];
then
 wget http://lammps.sandia.gov/tars/$LMP_VER.tar.gz
 tar -xzvf  $LMP_VER.tar.gz
fi
#
# SET THE Makefile SUFFIX. I USE intelmpi makefile 
#  
 SUFFIX=intelmpi
 cp $CURR_DIR/Makefile.${SUFFIX} $CURR_DIR/$LMP_VER/src/MAKE/
 cd $LMP_VER
#
# BUILD ADDITIONAL LIBRARIES
#
if [ $LIB_BUILD == "yes" ] 
then
     cd lib
#
echo " "
echo "************************ "
echo "* BUILDING THE atc LIB *"
echo "************************ "
     cp $CURR_DIR/atc-Makefile.mpiicpc $CURR_DIR/$LMP_VER/lib/atc/Makefile.mpiicpc
     cd atc 
# NOTE CREATED NEW MAKEFILE Makefile.mpiicpc
make -f Makefile.mpiicpc EXTRAMAKE=Makefile.lammps.empty CC=mpiicpc LINK=mpiicpc clean
make -f Makefile.mpiicpc EXTRAMAKE=Makefile.lammps.empty CC=mpiicpc LINK=mpiicpc
#
echo " "
echo "************************** "
echo "* BUILDING THE awpmd LIB *"
echo "************************** "
cd ../awpmd
make -f Makefile.mpicc EXTRAMAKE=Makefile.lammps.empty CC=icc clean
make -f Makefile.mpicc EXTRAMAKE=Makefile.lammps.empty CC=icc
#
echo " "
echo "**************************** "
echo "* BUILDING THE colvars LIB *"
echo "**************************** "
cd ../colvars
make -f Makefile.g++ EXTRAMAKE=Makefile.lammps.empty CXX=gcc clean
make -f Makefile.g++ EXTRAMAKE=Makefile.lammps.empty CXX=gcc
#
# THIS PART REQUIRES QM INTERFACE PWSCF -- HAVE TO TEST IF WORKS
#
#echo "************************* "
#echo "* BUILDING THE qmmm LIB *"
#echo "************************* "
#cd ../qmmm
#make -f Makefile.gfortran  MPICXX=mpicxx clean
#make -f Makefile.gfortran  MPICXX=mpicxx
#
echo " "
echo "************************* "
echo "* BUILDING THE REAX LIB * "
echo "************************* "
cd ../reax
make -f Makefile.gfortran F90=gfortran EXTRAMAKE=Makefile.lammps.gfortran clean
make -f Makefile.gfortran F90=gfortran EXTRAMAKE=Makefile.lammps.gfortran
#
echo " "
echo "************************* "
echo "*  BUILDING poems LIB   * "
echo "************************* "
cd ../poems
make -f Makefile.g++ CC=g++ clean
make -f Makefile.g++ CC=g++
#
echo " "
echo "********************** "
echo "* BUILDING meam MSCG * "
echo "********************** "
cd ../mscg
# wget https://github.com/uchicago-voth/MSCG-release/archive/master.zip
# unzip master.zip 
# cd MSCG-release-master
make -f Makefile."name" libmscg.a
#
echo " "
echo "********************* "
echo "* BUILDING meam LIB * "
echo "********************* "
cd ../meam
 make -f Makefile.gfortran  LINK=gcc F90=gfortran  clean
 make -f Makefile.gfortran  LINK=gcc F90=gfortran
#
#echo " "
#echo "********************* "
#echo "* BUILDING h5md LIB * "
#echo "********************* "
#
# HDF5 lib SHOULD BE LOADED if you want to build this
# Presently I do not build this bc it's extra overhead 
# and unless you need it I don't bother with it. 
# Note: I modified Makefile in h5md directory to use parallel h5cc -- h5pcc
#
# module load hdf5/1.8.16+intelmpi-5.1+intel-16.0
#cd ../h5md
#make clean
#make 
#
#echo " "
#echo "*********************** "
#echo "* BUILDING VORONI LIB * "
#echo "*********************** "
#cd ../voronoi
# THE FOLLOWING WAS DONE OUTSIDE THIS SCRIPT -- if you want this built you need to do this separtely
# ONE SHOULD INSTALL voro++ on in module system and simply create link
#wget http://math.lbl.gov/voro++/download/dir/voro++-0.4.6.tar.gz
#tar -xzvf voro++-0.4.6.tar.gz
#cd voro++-0.4.6
#vi config.mk  # HERE MODIFIED THE PREFIX TO POINT TO
#make clean
#make
#make install
# ln -sf /software/voro++-0.4.6-el7-x86_64/lib liblink
# ln -sf /software/voro++-0.4.6-el7-x86_64/include/voro++ includelink
#
echo " "
echo "******************** "
echo "* BUILDING SMD LIB *"
echo "******************** "
cd ../smd
# MAKE LINK TO EXISTING EIGEN BUILD IN THIS DIRECTORY
rm includelink
ln -sf /software/eigen-3.2-el7-x86_64/include/eigen3 includelink
cd ../
#
# END OF LIBRARIES BUILD
#
fi


#
# INCLUDE/EXCLUDE PACKAGES FROM THE BUILD
#
# CORE PACKAGES
#
echo " "
echo "******************** "
echo "* SETTING PACKAGES * "
echo "******************** "
echo " "
      cd $CURR_DIR/$LMP_VER/src
      make yes-all 
# NO GPU BUILD -- adjust accordingly if you want GPU+cuda
      make no-GPU
      make no-KIM
      make no-KOKKOS
      make no-PYTHON
      make no-voronoi
      make no-meam
      make no-mscg
#
# USER PACKAGES
#
      make yes-user
      make no-USER-OMP
      make no-USER-QUIP
      make no-USER-VTK
      make no-USER-QMMM
      make no-USER-H5MD
#
if [ $LIB_BUILD == "no" ] 
then
# DON'T BUILD ANY PACKAGES REQUIRING LIBRARY BUILDS
      make no-lib
echo " "
echo "********************************* "
echo "* NOT MAKING ANY MODULES W LIBS * "
echo "********************************* "
fi
#
# CLEAN UP ANY PREVIOUS BUILD OBJECT FILES ETC.
#
      make clean-all
#
# LIST PACKAGES TO BE BUILT/REMOVED
#
echo " "
echo "******************************** "
echo "* PACKAGES TO BE BUILT/REMOVED * "
echo "******************************** "
echo " "
      make package-status
#
# FINALLY BUILD THIS BEAST
#
CORES=14
make -j$CORES $SUFFIX
install -d $PREFIX/$LMP_VER/bin
install -m 755 $CURR_DIR/$LMP_VER/src/lmp_${SUFFIX} $PREFIX/$LMP_VER/bin
#
# EOF
#
